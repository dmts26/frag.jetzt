export const blue = {

  '--primary' : '#FFBA2C',
  '--primary-variant': '#2df7ff',

  '--secondary': '#DD005F',
  '--secondary-variant': '#6f74dd',

  '--background': '#f1feff',
  '--surface': '#2df7ff',
  '--dialog': '#d0d0d0',
  '--cancel': '#2df7ff',
  '--alt-surface': '#87a7ac',
  '--alt-dialog': '#455a64',

  '--on-primary': '#000000',
  '--on-secondary': '#FFFFFF',
  '--on-background': '#FFFFFF',
  '--on-surface': '#000000',
  '--on-cancel': '#000000',

  '--green': '#00b100',
  '--red': '#fe080d',
  '--yellow': '#ff9700',
  '--blue': '#2df7ff',
  '--purple': '#d838f3',
  '--light-green': '#93dc1a',
  '--grey': '#959595',
  '--grey-light': '#c6c6c6',
  '--black': '#040612',
  '--moderator': '#99c5da'
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'Blue Theme',
      'de': 'Blaues Theme'
    },
    'description': {
      'en': 'Blue theme compliant with WCAG 2.1 AA',
      'de': 'Blaues Theme nach WCAG 2.1 AA'
    }
  },
  'isDark': false,
  'order': 4,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'surface'

};
